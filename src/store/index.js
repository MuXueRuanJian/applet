import setting from "./setting.js";
import user from "./user.js";
import pages from "./pages.js";
import cart from "./cart.js";
import order from "./order.js";
import goods from "./goods.js";
import after from "./after.js";
import components from "./components.js";

export default {
    setting,
    user,
    pages,
    cart,
    order,
    goods,
    after,
    components
}